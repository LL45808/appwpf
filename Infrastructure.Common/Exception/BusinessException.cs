﻿using Furion.FriendlyException;
using System;
using System.Linq.Expressions;

namespace Infrastructure.Common 
{
    /// <summary>
    /// 业务异常！友好异常，不记录日志
    /// </summary>
    public class BusinessException : Exception
    {
        /// <summary>
        /// 1业务异常！友好异常，不记录日志;
        /// 2系统编码异常，服务异常，代码异常。记录日志;
        /// </summary>
        public BusinessErrorType BusinessType { get; set; }
        public BusinessException(string msg, BusinessErrorType type = BusinessErrorType.提示异常) : base(msg)
        {
            BusinessType = type;
        }
    }
    
    /// <summary>
    /// 全局异常
    /// </summary>
    public static class ExOops
    {
        /// <summary>
        /// 默认提示
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public static BusinessException BusinessOh(string error)
        {
            Serilog.Log.Error("【异常BusinessOh】" + error);
            //Oops.Oh(error);//AppFriendlyException
            return new BusinessException(error);
        }

        /// <summary>
        /// 系统异常,记录
        /// </summary>
        /// <param name="error"></param>
        /// <returns></returns>
        public static BusinessException BusinessOhLog(string error)
        {
            Serilog.Log.Error("【异常BusinessOhLog】"+ error);
            return new BusinessException(error, BusinessErrorType.系统异常);
        }

    }
}
