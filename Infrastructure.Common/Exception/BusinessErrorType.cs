﻿using System;
using System.Linq.Expressions;

namespace Infrastructure.Common 
{
    /// <summary>
    /// 业务异常 
    /// </summary>
    public enum BusinessErrorType
    {
        /// <summary>
        /// 提示异常
        /// </summary>
        提示异常 = 1,
        /// <summary>
        /// 系统异常
        /// </summary>
        系统异常 = 2,
    }

}
