﻿using System;
using Yitter.IdGenerator;

namespace Infrastructure.Common 
{
    /// <summary>
    /// 雪花ID
    /// </summary>
    public class SnowFlake
    {
        private static readonly DateTime dateTime = new DateTime(2021, 10, 1, 11, 45, 2, 20, DateTimeKind.Utc);
        /// <summary>
        /// 不能创建实例
        /// </summary>
        private SnowFlake() { }

        static SnowFlake()
        {

        }
        /// <summary>
        /// 1个项目，只设置1次
        /// </summary>
        /// <param name="workerId"></param>
        public static void Set(ushort workerId)
        {
            YitIdHelper.SetIdGenerator(new IdGeneratorOptions
            {
                WorkerId = workerId,
                BaseTime = dateTime
            });
        }

        public static long NextId => YitIdHelper.NextId();
    }
}
