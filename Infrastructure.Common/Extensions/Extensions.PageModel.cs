﻿using System.Linq;
using Infrastructure.Common;
using Infrastructure.Common.Attributes;

namespace Infrastructure.Common
{
    public static partial class Extensions
    {
        /// IQueryable<TEntity>
        public static PageModel<T> CreatePageModel<T>(this IOrderedEnumerable<T> IQModle, PageModel pageModel) where T : new()
        {
            var list2 = IQModle.Skip(pageModel.PageSize * (pageModel.PageIndex - 1)).Take(pageModel.PageSize);
            return new PageModel<T>
            {
                Data = list2.ToList(),
                PageIndex = pageModel.PageIndex,
                PageSize = pageModel.PageSize,
                TotalDataCount = IQModle.Count(),
                TabColumns = DisplayFieldMethod.GetColDefs<T>()
            };
        }


       
            



    }
}
