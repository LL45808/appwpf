﻿using System;
using Infrastructure.Common;
using Enum = Infrastructure.Common.Enum;

namespace Infrastructure.Common
{
    /// <summary>
    /// 系统扩展 - 公共
    /// </summary>
    public static partial class Extensions
    {
        /// <summary>
        /// 安全获取值，当值为null时，不会抛出异常
        /// </summary>
        /// <param name="value">可空值</param>
        public static T SafeValue<T>(this T? value) where T : struct
        {
            return value ?? default(T);
        }

        /// <summary>
        /// 获取枚举值
        /// </summary>
        /// <param name="instance">枚举实例</param>
        public static int Value(this System.Enum instance)
        {
            if (instance == null)
                return 0;
            return Enum.GetValue(instance.GetType(), instance);
        }

        /// <summary>
        /// 获取枚举值
        /// </summary>
        /// <typeparam name="TResult">返回值类型</typeparam>
        /// <param name="instance">枚举实例</param>
        //public static TResult Value<TResult>( this System.Enum instance ) {
        //    if( instance == null )
        //        return default( TResult );
        //    return Convert.To<TResult>( Value( instance ) );
        //}

        /// <summary>
        /// 获取枚举描述,使用System.ComponentModel.Description特性设置描述
        /// </summary>
        /// <param name="instance">枚举实例</param>
        public static string Description(this System.Enum instance)
        {
            if (instance == null)
                return string.Empty;
            var message = Enum.GetDescription(instance.GetType(), instance);
            if (message.IsNotNull())
                return message;

            return instance.ToString();
        }

        /// <summary>
        /// 有序Guid， MySQL、PostgreSQL、SQLite 使用
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static Guid NewSequentialGuid(this System.Guid instance)
        {
            return GuidHelper.NewSequentialGuid();
        }

        /// <summary>
        ///  计算百分比
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static double ToPercentage(this object num, object totalnum)
        {
            var mem = (Convertes.ToDouble(num, 2) / Convertes.ToDouble(totalnum, 2));
            var useMem = Convertes.ToDouble(mem * 100, 2);
            return useMem;
        }


    }
}
