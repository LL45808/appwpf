﻿using Infrastructure.Common;

namespace Infrastructure.Common
{
    public static partial class  Extensions
    {
        /// <summary>
        /// 将对象转换为Json字符串
        /// </summary>
        /// <param name="target">目标对象</param>
        /// <param name="isConvertToSingleQuotes">是否将双引号转成单引号</param>
        public static string ToJson(this object target)
        {
            return Json.ToJson(target);
        }

        /// <summary>
        /// json字符 转为对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T ToObject<T>(this string json)
        {
            return Json.ToObject<T>(json);
        }

        /// <summary>
        /// 转为对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T ToObject<T,K>(this K obj) where T:new() where K : new()
        {
            return Json.ToObject<T>(obj.ToJson());
        }

        /// <summary>
        /// 转为对象 不抛异常 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T ToObjectTry<T, K>(this K obj) where T : new() where K : new()
        {
            if (obj == null)
            {
                return default;
            }
            try
            {
                return Json.ToObject<T>(obj.ToJson());
            }
            catch (System.Exception)
            {
                return default;
            }
           
        }

    }
}
