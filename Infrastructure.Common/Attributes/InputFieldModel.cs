﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using Infrastructure.Common;

namespace Infrastructure.Common.Attributes
{

    /// <summary>
    /// 输入参数 sql化 
    /// 用于标识查询 sql 拼接
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class InputSqlFieldModel : Attribute
    {

        public InputSqlFieldModel(string from = "", string fieldName = null)
        {
            From = from;
            FieldName = fieldName;
        }

        /// <summary>
        /// 字段名称
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// 属于那个表名字 "a or table"
        /// </summary>
        public string From { get; set; }

        public string NetType { get; set; }


        /// <summary>
        ///  
        /// </summary>
        public object Value { get; set; }

        public PropertyInfo Property { get; set; }

        public string GetColumn()
        {
            return From.IsNotNull() ? $" {From}.{FieldName} " : FieldName;
        }

    }

    public class InputSqlFieldMethod<T> where T : BaseAntdInput, new()
    {
        private List<InputSqlFieldModel> Model { get; }

        private StringBuilder SqlFrom { get; set; }

        private T input { get; set; }

        public InputSqlFieldMethod(T input, string sqlfrom)
        {

            this.input = input;
            SqlFrom = new StringBuilder(sqlfrom);
            Model = Cols();


        }

        private List<InputSqlFieldModel> Cols()
        {
            var dic = new List<InputSqlFieldModel>();
            foreach (PropertyInfo p in typeof(T).GetProperties())
            {
                foreach (object attribute in p.GetCustomAttributes(true))
                {
                    if (attribute is InputSqlFieldModel)
                    {
                        InputSqlFieldModel attr = (InputSqlFieldModel)attribute;
                        if (string.IsNullOrWhiteSpace(attr.FieldName))
                        {
                            attr.FieldName = p.Name;
                        }

                        attr.Property = p;
                        attr.NetType = CodeHelper.ConvertDataType(p.PropertyType);
                        attr.Value = p.GetValue(input);
                        dic.Add(attr);
                    }
                }
            }
            return dic.ToList();//.OrderBy(x => x.FieldName)
        }

        public InputSqlFieldMethod<T> Where(string @where = null)
        {
            if (@where.IsNull())
            {
                SqlFrom.Append(" where 1 = 1  ");
            }
            else
            {
                SqlFrom.Append(@where);
            }


            foreach (var item in Model)
            {
                if (item.Value == null || item.Value.ToString() == "" || item.Value.ToString() == "0")
                {
                    continue;
                }
                if (item.Value.ToString() == DateTime.MinValue.ToString())
                {
                    continue;
                }

                //  case "System.Guid": return "guid";
                //case "System.String": return "string";
                //case "System.Int32": return "int";
                //case "System.Int64": return "Guid";
                //case "System.Single": return "float";
                //case "System.Double": return "double";
                //case "System.Decimal": return "decimal";
                //case "System.Boolean": return "bool";
                //case "System.DateTime": return "DateTime";
                //case "System.DateTime": return "DateTime";
                //case "System.Byte": return "byte";
                //case "System.Byte[]": return "byte[]";
                // where += " AND POSITION(@Name IN a.Name) ";
                switch (item.NetType)
                {
                    case "string":
                        SqlFrom.Append($" and  POSITION(@{item.FieldName} IN {item.GetColumn()}) ");
                        break;
                    case "DateTime":
                        var date = item.Value.ToString().ToDate();
                        var start = date.CurrentDayStart();
                        var end = date.CurrentDayEnd();
                        SqlFrom.Append($" and {item.GetColumn()} >='{start}' and {item.GetColumn()} <='{end}' ");
                        break;
                    case "Guid":
                    case "int":
                    case "double":
                    case "decimal":
                        SqlFrom.Append($" and {item.GetColumn()} = @{item.FieldName} ");
                        break;
                    case "bool":
                        break;
                    default:
                        break;
                }


                //if (input.OrderNo.IsNotNull())
                //{
                //    sql += $" and b.OrderNo  like '%{input.OrderNo}%' ";
                //}

                //if (input.ReceiveUnitCode.IsNotNull())
                //{
                //    sql += $" and b.ReceiveUnitCode = '{input.ReceiveUnitCode}' ";
                //}

                //if (input.Status > 0)
                //{
                //    sql += $" and  a.`Status` = '{input.Status}' ";
                //}
                //sql += $" order by  a.Id desc ";

            }
            return this;
        }

        public InputSqlFieldMethod<T> GroupBy(string @groupBy = null)
        {
            if (@groupBy.IsNotNull())
            {
                SqlFrom.Append(@groupBy);
            }
            return this;
        }



        /// <summary>
        /// 排序方式(默认降序)
        /// </summary>
        /// <param name="pageInput"></param>
        /// <param name="descSort">是否降序</param>
        /// <returns></returns>
        public InputSqlFieldMethod<T> OrderBuilder(bool descSort = true, params string[] orderField)
        {

            // 排序是否可用-排序字段和排序顺序都为非空才启用排序
            if (orderField.Length == 0 && !string.IsNullOrEmpty(input.SortField) && !string.IsNullOrEmpty(input.SortOrder))
            {
                var orderStr = $"{input.SortField} {(input.SortOrder == input.DescStr ? "Desc" : "Asc")}";
                SqlFrom.Append($" order by {orderStr} ");
                return this;
            }

            // 约定默认每张表都有Id排序
            var orderFields = " Id ";
            if (orderField.Length > 0)
            {
                orderFields = string.Join(",", orderField);
            }

            SqlFrom.Append($" order by {orderFields} {(descSort ? " Desc " : " Asc ")}");
            return this;
        }


        public InputSqlFieldMethod<T> Page()
        {
            SqlFrom.Append($" limit {(input.PageNo - 1) * input.PageSize},{input.PageSize} ; SELECT FOUND_ROWS();");
            return this;
        }



        public string ToSql()
        {
            return SqlFrom.ToString();
        }

        public StringBuilder Build()
        {
            return SqlFrom;
        }
    }

}
