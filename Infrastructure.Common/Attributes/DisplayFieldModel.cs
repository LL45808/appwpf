﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Infrastructure.Common;

namespace Infrastructure.Common.Attributes
{
    /// <summary>
    /// 表头
    /// </summary>
    public class DisplayColumns
    {
        public DisplayColumns(string displayName, string fieldName)
        {
            Title = displayName;
            DataIndex = fieldName;
        }
        public string Title { get; set; }

        public string Align { get; set; } = "center";

        public string DataIndex { get; set; }
    }


    /// <summary>
    /// 自定义实体属性、导出
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class DisplayFieldModel : DescriptionAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="displayName"></param>
        /// <param name="fieldType"></param>
        /// <param name="fieldName"></param>
        /// <param name="visible"></param>
        /// <param name="export">是否导出</param>
        /// <param name="Isfixed"></param>
        public DisplayFieldModel(string displayName = "",
                                    int index = 0,
                                    string fieldType = null,
                                    string fieldName = null,
                                    bool visible = true,
                                    bool export = true,
                                    bool Isfixed = false) : base(displayName)
        {
            FieldName = fieldName;
            DisplayName = displayName;
            Visible = visible;
            IsFixed = Isfixed;
            FieldType = fieldType;
            Export = export;
            Index = index;
        }

        /// <summary>
        ///  排序
        /// </summary>
        public int Index { set; get; }

        /// <summary>
        /// 是否导出
        /// </summary>
        public bool Export { set; get; }

        /// <summary>
        /// 样式--  是否固定
        /// </summary>
        public bool IsFixed { set; get; }
        /// <summary>
        /// 字段名称
        /// </summary>
        public string FieldName { get; set; }
        /// <summary>
        /// 显示名称
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        ///  是否可见
        /// </summary>
        public bool Visible { get; set; }

        /// <summary>
        ///  字段类型
        /// </summary>
        public string FieldType { get; set; }

        public override object TypeId => FieldType;

        public PropertyInfo Property { get; set; }

    }


    /// <summary>
    ///1.通过映射，找到成员属性关联的特性类实例，
    /// </summary>
    public static class DisplayFieldMethod
    {

        private static List<DisplayFieldModel> Cols<T>() where T : new()
        {
            var dic = new List<DisplayFieldModel>();
            foreach (PropertyInfo p in typeof(T).GetProperties())
            {
                foreach (object attribute in p.GetCustomAttributes(true)) //2.通过映射，找到成员属性上关联的特性类实例，
                {
                    if (attribute is DisplayFieldModel)//3.如果找到了限定长度的特性类对象，就用这个特性类对象验证该成员
                    {
                        DisplayFieldModel attr = (DisplayFieldModel)attribute;
                        if (string.IsNullOrWhiteSpace(attr.FieldName))
                        {
                            attr.FieldName = p.Name;
                        }
                        if (string.IsNullOrWhiteSpace(attr.FieldType))
                        {
                            attr.FieldType = p.PropertyType.Name;
                        }
                        //if (p.PropertyType is  Nullable) { }

                        if (attr.FieldType.Contains("Nullable"))
                        {
                            try
                            {
                                // System.Nullable`1[System.Decimal] EB
                                attr.FieldType = p.ToString().Split('[')[1].Split(']')[0].Split('.').Last();
                            }
                            catch
                            {

                            }
                        }
                        attr.Property = p;
                        dic.Add(attr);
                    }
                }
            }
            return dic.ToList();//.OrderBy(x => x.FieldName)
        }



        public static List<DisplayColumns> GetColumns<T>() where T : new()
        {
            var cols = Cols<T>().Where(x => x.Visible).ToList();
            var list = cols.Select(x => new DisplayColumns(x.DisplayName.FirstLowerCase(), x.FieldName.FirstLowerCase()));
            return list.ToList();
        }


        /// <summary>
        /// 表格表头
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static List<DisplayFieldModel> GetColDefs<T>() where T : new()
        {
            var cols = Cols<T>().Where(x => x.Visible).ToList();
            return cols;
        }
        /// <summary>
        /// vue 表格数据 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <returns></returns>
        public static List<DisplayFieldModel> GetColDefs<T>(this T t) where T : new()
        {
            return GetColDefs<T>();
        }

        /// <summary>
        /// 获取导出表格数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<DisplayFieldModel> GetExportColDefs<T>() where T : new()
        {
            var cols = Cols<T>().Where(x => x.Export).ToList();
            return cols;
        }

    }

}
