﻿namespace Infrastructure.Common
{
    /// <summary>
    /// Api返回参数格式
    /// </summary>
    public interface IResultModel<T>
    {
        /// <summary>
        /// 执行成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 状态码
        /// </summary>
        public int? Code { get; set; }

        /// <summary>
        /// 错误信息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 数据
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// 附加数据
        /// </summary>
        public object Extras { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        public long Timestamp { get; set; }

    }

    /// <summary>
    /// Api返回参数格式
    /// </summary>
    public class ResultModel<T> : IResultModel<T>
    {
        /// <summary>
        /// 默认成功
        /// </summary>
        public ResultModel()
        {

        }
        /// <summary>
        /// 默认成功
        /// </summary>
        public ResultModel(T t)
        {
            Data = t;
        }

        public T Data { set; get; }

        public string Message { get; set; } = "Success";
        public bool Success { get; set; } = true;
        public int? Code { get; set; } = 200;
        public object Extras { get; set; }
        public long Timestamp { get; set; } = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

    }

}
