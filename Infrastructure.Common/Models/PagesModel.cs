﻿using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Common
{

    /// <summary>
    /// 查询排序模型
    /// </summary>
    public class QuerySortModel
    {
        /// <summary>
        /// 字段
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// 排序类型
        /// </summary>
        public SortType Type { get; set; }
    }


    /// <summary>
    /// 排序规则
    /// </summary>
    public enum SortType
    {
        /// <summary>
        /// 升序
        /// </summary>
        Asc,
        /// <summary>
        /// 降序
        /// </summary>
        Desc
    }


    public class ParamObj
    {

        //需要序列化回去 DynamicFilterInfo dyfilter  
        public object DyFilter { get; set; }

        public List<QuerySortModel> OrderBy { get; set; } = new List<QuerySortModel>();
    }


    /// <summary>
    /// 分页参数类
    /// </summary>
    public class PageModel
    {

        /// <summary>
        /// 依次排序
        /// </summary>
        public List<QuerySortModel> SortList { get; set; }

        public PageModel()
        {
            PageIndex = 1;
            PageSize = 8;

        }

        public ParamObj ParamObj { get; set; } = new ParamObj();

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }

    /// <summary>
    /// 通用分页信息类
    /// </summary>
    public class PageModel<T> : PageModel
    {
        /// <summary>
        /// 总页数
        /// </summary>
        public int TotalPageCount => (TotalDataCount % PageSize) > 0 ? (TotalDataCount / PageSize) + 1 : TotalDataCount / PageSize;

        /// <summary>
        /// 数据总数
        /// </summary>
        public int TotalDataCount { get; set; }

        /// <summary>
        /// 返回数据
        /// </summary>
        public List<T> Data { get; set; }

        /// <summary>
        /// 表头列
        /// </summary>
        public object TabColumns { get; set; }


        /// <summary>
        /// 内存分页
        /// </summary>
        /// <typeparam name="TPageModel"></typeparam>
        /// <param name="allList">所有数据</param>
        /// <param name="tPage">分页参数</param>
        public void Paging<TPageModel>(List<T> allList, TPageModel tPage) where TPageModel : PageModel
        {
            PageIndex = tPage.PageIndex;
            PageSize = tPage.PageSize;
            TotalDataCount = allList.Count();
            Data = allList.Skip((PageIndex - 1) * PageSize).Take(PageSize).ToList();
        }

    }

}
