﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using System;
using System.IO;
using System.Text;

namespace Infrastructure.Common
{

    /// <summary>
    ///  Program
    /// </summary>
    public static class WebHostBuilderExtensions
    {
        /// <summary>
        ///  使用
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        //public static IHostBuilder UseLogging(this IHostBuilder builder)
        //{
        //    //http://www.mamicode.com/info-detail-2900184.html
        //    var pathWarning = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs/warning", "warning.txt");
        //    var pathError = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs/error", "error.txt");
        //    string template = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{SourceContext:l} {Level:u3}] {Message:lj}{NewLine}{Exception}";
        //    builder.UseSerilog((hostingContext, loggerConfiguration) =>
        //    {
        //        loggerConfiguration.WriteTo.Console()
        //            .WriteTo.Logger(lc => lc
        //                .Filter.ByIncludingOnly(x => x.Level == LogEventLevel.Error)
        //                .WriteTo.File(
        //                    pathError,
        //                    outputTemplate: template,
        //                    rollingInterval: RollingInterval.Day,
        //                    shared: true,
        //                    fileSizeLimitBytes: 1 * 1024 * 1024,
        //                    rollOnFileSizeLimit: true))
        //            .WriteTo.Logger(lc => lc
        //                .Filter.ByIncludingOnly(x => x.Level == LogEventLevel.Warning)
        //                .WriteTo.File(
        //                    pathWarning,
        //                    outputTemplate: template,
        //                    rollingInterval: RollingInterval.Day,
        //                    shared: true,
        //                    fileSizeLimitBytes: 1 * 1024 * 1024,
        //                    rollOnFileSizeLimit: true));
        //    });
        //    return builder;
        //}

        /// <summary>
        /// 添加异常Error,写日志文件
        /// </summary>
        /// <param name="loggerConfiguration"></param>
        /// <returns></returns>
        public static LoggerConfiguration AddErrorLog(this LoggerConfiguration loggerConfiguration)
        {
            string template = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{SourceContext:l} {Level:u3}] {Message:lj}{NewLine}{Exception}";
            //.WriteTo.Console()
            // .WriteTo.Logger(lc => lc
            //loggerConfiguration.WriteTo.Console(encoding: Encoding.UTF8);
            loggerConfiguration.WriteTo
                .Logger(lc =>
                            lc.Filter
                               .ByIncludingOnly(x => x.Level == LogEventLevel.Error)
                                   .WriteTo.File(
                                   "logs/error/.log",
                                   outputTemplate: template,
                                   rollingInterval: RollingInterval.Day,
                                   shared: true,
                                   fileSizeLimitBytes: 1 * 1024 * 1024,
                                   rollOnFileSizeLimit: true, encoding: Encoding.UTF8)
                               .Filter.ByIncludingOnly(x => x.Level == LogEventLevel.Warning)
                                 .WriteTo.File(
                                   "logs/warn/.log",
                                   outputTemplate: template,
                                   rollingInterval: RollingInterval.Day,
                                   shared: true,
                                   fileSizeLimitBytes: 1 * 1024 * 1024,
                                   rollOnFileSizeLimit: true, encoding: Encoding.UTF8)
                               );

            //

            return loggerConfiguration;
        }

    }
}
