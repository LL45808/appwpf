﻿namespace Infrastructure.Common
{
    /// <summary>
    /// 表头
    /// </summary>
    public class AntTableColumns
    {

        public AntTableColumns()
        {
        }

        public AntTableColumns(string displayName, string fieldName)
        {
            Title = displayName;
            DataIndex = fieldName;
        }

        public string Title { get; set; }

        public string Align { get; set; } = "center";

        public string DataIndex { get; set; }

        private List<AntTableColumns> DataColumns { get; set; } = new();

        public AntTableColumns SetColumn(string displayName, string fieldName)
        {
            fieldName = fieldName?.Trim() ?? "";
            if (!DataColumns.Any(x => x.DataIndex == fieldName))
            {
                DataColumns.Add(new AntTableColumns(displayName, fieldName));
            }
            return this;
        }

        public List<AntTableColumns> GetColumns()
        {
            return DataColumns;
        }

    }

}
