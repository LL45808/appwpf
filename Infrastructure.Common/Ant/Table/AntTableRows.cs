﻿namespace Infrastructure.Common
{
    /// <summary>
    /// 表头
    /// </summary>
    public class AntTableRows
    {
        public Guid RowId { get; set; }

        private List<Item> DataColumns { get; set; } = new();
        public AntTableRows()
        {
            RowId = Guid.NewGuid();
        }

        public AntTableRows AddColumn(string fieldName, string Value)
        {
            fieldName = fieldName?.Trim() ?? "";
            DataColumns.Add(new Item(fieldName, Value));
            return this;
        }

        public List<Item> GetColumns()
        {
            return DataColumns;
        }

    }

}
