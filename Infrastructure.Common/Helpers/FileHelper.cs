﻿using System;
using System.IO;
using System.Text;

namespace Infrastructure.Common
{
    /// <summary>
    /// 文件操作帮助类
    /// </summary>
    public class FileHelper
    {
        #region 读操作

        /// <summary>
        /// 判断文件是否存在
        /// </summary>
        /// <param name="path">文件目录</param>
        /// <returns></returns>
        public static bool Exists(string path)
        {
            return System.IO.File.Exists(path);
        }

        /// <summary>
        /// 获取当前程序根目录
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentDir()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        #endregion

        #region 写操作

        /// <summary>
        /// 输出字符串到文件
        /// 注：使用系统默认编码;若文件不存在则创建新的,若存在则覆盖
        /// </summary>
        /// <param name="content">内容</param>
        /// <param name="path">文件路径</param>
        public static void WriteTxt(string content, string path)
        {
            WriteTxt(content, path, null, null);
        }

        /// <summary>
        /// 输出字符串到文件
        /// 注：使用自定义编码;若文件不存在则创建新的,若存在则覆盖
        /// </summary>
        /// <param name="content">内容</param>
        /// <param name="path">文件路径</param>
        /// <param name="encoding">编码</param>
        public static void WriteTxt(string content, string path, Encoding encoding)
        {
            WriteTxt(content, path, encoding, null);
        }

        /// <summary>
        /// 输出字符串到文件
        /// 注：使用自定义模式,使用UTF-8编码
        /// </summary>
        /// <param name="content">内容</param>
        /// <param name="path">文件路径</param>
        /// <param name="fileModel">输出方法</param>
        public static void WriteTxt(string content, string path, FileMode fileModel)
        {
            WriteTxt(content, path, Encoding.UTF8, fileModel);
        }

        /// <summary>
        /// 输出字符串到文件
        /// 注：使用自定义编码以及写入模式
        /// </summary>
        /// <param name="content">内容</param>
        /// <param name="path">文件路径</param>
        /// <param name="encoding">字符编码</param>
        /// <param name="fileModel">写入模式</param>
        public static void WriteTxt(string content, string path, Encoding encoding, FileMode fileModel)
        {
            WriteTxt(content, path, encoding, (FileMode?)fileModel);
        }

        /// <summary>
        /// 输出字符串到文件
        /// 注：使用自定义编码以及写入模式
        /// </summary>
        /// <param name="content">内容</param>
        /// <param name="path">文件路径</param>
        /// <param name="encoding">字符编码</param>
        /// <param name="fileModel">写入模式</param>
        private static void WriteTxt(string content, string path, Encoding encoding, FileMode? fileModel)
        {
            encoding = encoding ?? Encoding.UTF8;
            fileModel = fileModel ?? FileMode.Create;
            string dir = Path.GetDirectoryName(path);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            using (FileStream fileStream = new FileStream(path, fileModel.Value))
            {
                using (StreamWriter streamWriter = new StreamWriter(fileStream, encoding))
                {
                    streamWriter.Write(content);
                    streamWriter.Flush();
                }
            }
        }

        ///// <summary>
        ///// 输出日志到指定文件
        ///// </summary>
        ///// <param name="msg">日志消息</param>
        ///// <param name="path">日志文件位置（默认为D:\测试\a.log）</param>
        //public static void WriteLog(string msg, string path = @"Log.txt")
        //{
        //    string content = $"{DateTime.Now.ToCstTime().ToString("yyyy-MM-dd HH:mm:ss")}:{msg}";

        //    WriteTxt(content, $"{GetCurrentDir()}{content}");
        //}

        #endregion


        /// <summary>
        ///  写日志。保存运行日志到程序运行文件夹下，并以当前日期命名
        /// </summary>
        public static void Write(string logText, string type = "error")
        {
            Console.WriteLine(logText);
            string mainPath = Directory.GetCurrentDirectory();
            StringBuilder GetAppPath = new StringBuilder().Append(mainPath);
            //保存运行日志到程序运行文件夹下，并以当前日期命名
            GetAppPath.Append($@"/logs/{DateTime.Now:yyyyMMdd}/{type}.txt");
            var strPath = GetAppPath.ToString();
            NewCreate(strPath);
            var sw = new StreamWriter(strPath, true, Encoding.UTF8);

            //sw.WriteLine($"=======【{DateTime.Now:yyyy-MM-dd HH:mm:ss}】=========");
            sw.WriteLine(logText);
            //销毁数据数据流通道
            sw.Dispose();
        }

        /// <summary>
        /// 验证存在文件 没有就创建
        /// </summary>
        /// <param name="path"></param>
        private static void NewCreate(string path)
        {
            //path，并判断不存在是创建文件夹
            if (!Directory.Exists(Path.GetDirectoryName(path)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(path));
            }
            //判断文件不存在时，创建该文件
            if (!System.IO.File.Exists(path))
            {
                //创建完毕后，需关闭该IO通道，以使后续读写可继续进行
                System.IO.File.Create(path).Close();
            }

        }
    }

}
