﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Common
{
    /// <summary>
    /// 程序集的类
    /// </summary>
    public static class AssemblyHelper
    {

        private static object _lock = new object();

        /// <summary>
        /// 框架所有类
        /// </summary>
        public static List<Type> GetTypeAll
        {
            get
            {
                List<Type> _allFxTypes = new List<Type>();
                lock (_lock)
                {
                    Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll")
                        .Select(x => Assembly.LoadFrom(x))
                        .Where(x => !x.IsDynamic).ToList()
                        .ForEach(aAssembly =>
                        {
                            try
                            {
                                _allFxTypes.AddRange(aAssembly.GetTypes());
                            }
                            catch
                            {

                            }
                        });
                }

                return _allFxTypes;
            }
        }

        /// <summary>
        /// 程序集头
        /// </summary>
        /// <param name="withAssembly">程序集头的标识</param>
        /// <returns></returns>
        public static List<Assembly> GetAssembly(string withAssembly)
        {

            lock (_lock)
            {
                return Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.dll")
                    .Where(x => x.Contains(withAssembly))
                    .Select(x => Assembly.LoadFrom(x)).ToList();
            }
        }

        /// <summary>
        /// 程序集头d的所有类型
        /// </summary>
        /// <param name="withAssembly">程序集头的标识</param>
        /// <returns></returns>
        public static List<Type> GetTypes(string withAssembly)
        {
            var types = new List<Type>();
            lock (_lock)
            {
                var assemblies = GetAssembly(withAssembly);
                foreach (var item in assemblies)
                {
                    var ts = item.GetExportedTypes();
                    types.AddRange(ts);
                }
                return types;
            }
        }

    }


}
