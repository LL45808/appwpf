﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure.Common;

namespace Infrastructure.Common
{
    /// <summary>
    /// Http请求帮助类
    /// </summary>
    public static class UploadImgHelper
    {
        private static readonly ServiceProvider _serviceProvider =
            new ServiceCollection().AddHttpClient().BuildServiceProvider();

        /// <summary>
        /// 上传文件
        /// </summary>
        /// <returns></returns>
        public static async Task<string> UploadFile(Stream content, string pathType, string path = "")
        {
            string resultUrl = "";
            try
            {
                var de = getEx(content);
                var file = await FileHelpers.ToBytesAsync(content);
                if (file.Length > 0)
                {
                    resultUrl = $"{Guid.NewGuid().ToString().ToMD5String()}.{de.ToString().ToLower()}";
                    var uploadUrl = $"{pathType}/{resultUrl}";
                    //保存到磁盘
                    SaveToDisk(file, uploadUrl);

                }

            }
            catch (Exception)
            {

                return "";
            }

            if (path == "")
            {
                return resultUrl;
            }

            return $"{path}/{resultUrl}";
        }


        public static async Task<string> GetUploadImage(string url, string pathType, string path = "")
        {
            string resultUrl = "";
            if (url.IsNull())
            {
                return resultUrl;
            }

            try
            {
                var httpClientFactory = _serviceProvider.GetService<IHttpClientFactory>();
                var client = httpClientFactory.CreateClient();
                var response = await client.SendAsync(new HttpRequestMessage(System.Net.Http.HttpMethod.Get, url));
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStreamAsync();
                    var de = getEx(content);

                    var file = await FileHelpers.ToBytesAsync(content);
                    if (file.Length > 0)
                    {
                        resultUrl = $"{url.ToMD5String()}.{de.ToString().ToLower()}";
                        var uploadUrl = $"{pathType}/{resultUrl}";
                        //保存到磁盘
                        SaveToDisk(file, uploadUrl);

                    }
                }

            }
            catch
            {
                resultUrl = "";
            }

            if (path == "")
            {
                return resultUrl;
            }
            return $"{path}/{resultUrl}";

        }



        /// <summary>
        /// 保存到磁盘
        /// </summary>
        /// <param name="bytes">字节数组</param>
        /// <param name="saveFullPath">全路径</param>
        /// <returns></returns>
        public static void SaveToDisk(byte[] bytes, string saveFullPath)
        {
            var fullPath = Path.GetDirectoryName(saveFullPath);
            //如果没有此文件夹，则新建
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }
            //创建文件，返回一个 FileStream，它提供对 path 中指定的文件的读/写访问。
            using FileStream stream = File.Create(saveFullPath);
            //将字节数组写入流
            stream.Write(bytes, 0, bytes.Length);
            stream.Close();
        }

        /// <summary>
        /// 获取流文件格式
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private static FileExtension getEx(Stream stream)
        {
            FileExtension extension;
            try
            {
                BinaryReader reader = new BinaryReader(stream);
                string fileclass = "";
                for (int i = 0; i < 2; i++)
                {
                    fileclass += reader.ReadByte().ToString();
                }
                extension = Enum.Parse<FileExtension>(fileclass);
            }
            catch
            {
                extension = FileExtension.VALIDFILE;
            }
            return extension;
        }

        public enum FileExtension
        {
            JPG = 255216,
            GIF = 7173,
            BMP = 6677,
            PNG = 13780,
            COM = 7790,
            EXE = 7790,
            DLL = 7790,
            RAR = 8297,
            ZIP = 8075,
            XML = 6063,
            HTML = 6033,
            ASPX = 239187,
            CS = 117115,
            JS = 119105,
            TXT = 210187,
            SQL = 255254,
            BAT = 64101,
            BTSEED = 10056,
            RDP = 255254,
            PSD = 5666,
            PDF = 3780,
            CHM = 7384,
            LOG = 70105,
            REG = 8269,
            HLP = 6395,
            DOC = 208207,
            XLS = 208207,
            DOCX = 208207,
            XLSX = 208207,
            VALIDFILE = 9999999
        }


    }
}
