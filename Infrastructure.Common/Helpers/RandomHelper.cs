﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Infrastructure.Common
{
    /// <summary>
    /// Random随机数帮助类
    /// </summary>
    public static class RandomHelper
    {

        public static object _lock = new object();
        private static Random _random { get; } = new Random();

        /// <summary>
        /// 下一个随机数
        /// </summary>
        /// <param name="minValue">最小值</param>
        /// <param name="maxValue">最大值</param>
        /// <returns></returns>
        public static int Next(int minValue, int maxValue)
        {
            return _random.Next(minValue, maxValue);
        }

        /// <summary>
        /// 下一个随机值
        /// </summary>
        /// <typeparam name="T">值类型</typeparam>
        /// <param name="source">值的集合</param>
        /// <returns></returns>
        public static T Next<T>(IEnumerable<T> source)
        {
            return source.ToList()[Next(0, source.Count())];
        }

        /// <summary>
        /// 单号生成
        /// </summary>
        /// <param name="winth"></param>
        /// <returns></returns>
        public static string GetNo(string winth = "")
        {
            winth = winth.ToUpper();
            lock (_lock)
            {
                Random ran = new Random();
                return winth + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ran.Next(1000, 9999).ToString();
            }
        }

        /// <summary>
        /// 单号生成
        /// </summary>
        /// <param name="winth"></param>
        /// <returns></returns>
        public static string GetTenantCode(string winth = "C")
        {
            winth = winth.ToUpper();
            lock (_lock)
            {
                Random ran = new Random();
                return winth + DateTime.Now.ToString("yyMM") + ran.Next(1000, 9999).ToString();
            }
        }

        /// <summary>
        /// 唯一码
        /// </summary>
        /// <param name="winth"></param>
        /// <returns></returns>
        public static string GetHighCode(string winth = "UN")
        {
            return GetNo(winth);
        }

        /// <summary>
        /// 单号生成
        /// </summary>
        /// <param name="winth"></param>
        /// <returns></returns>
        public static string[] GetNos(string winth = "", int counts = 10)
        {
            var d = Next(1000, 9999);
            var date = DateTime.Now;
            DateTime startTime = new DateTime(date.Year, date.Month, date.Day).ToLocalTime();
            var s = (int)(date - startTime).TotalSeconds;

            winth = winth.ToUpper();
            var list = new List<string>();
            var den = winth + DateTime.Now.ToString("yyMMddHHmm");
            var leng = 5;
            for (int i = 0; i < counts; i++)
            {
                list.Add(den + GetStr(i, 5));
            }
            return list.ToArray();
        }



        private static string GetStr(int num, int length = 5)
        {
            var count = length - num.ToString().Length;
            var str = "";
            for (int i = 0; i < count; i++)
            {
                str += "0";
            }
            str += num.ToString();
            return str;
        }


        /// <summary>
        /// 生成格子号
        /// </summary>
        /// <returns></returns>
        public static string GetLatticeCode()
        {
            return RandomUtil.Instance.GetRandWord(4);
        }
    }

    /// <summary>
    /// ** 描述：根据条件生成随机字符串
    /// </summary>
    public class RandomUtil
    {
        public static RandomUtil Instance => new RandomUtil();
        private readonly char[] RandChar =
        { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
         'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y',
         'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
          'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
        private int seed = 1;
        //private RandomSugar()
        //{
        //}
        /// <summary>
        /// 获取随机汉字数组
        /// </summary>
        /// <param name="strLength">数组长度</param>
        /// <returns></returns>
        public string GetRandChinese(int strLength = 1)
        {
            //定义一个字符串数组储存汉字编码的组成元素 
            string[] rBase = new String[16]
            { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

            Random rnd = new Random(Guid.NewGuid().GetHashCode());

            //定义一个object数组用来 
            object[] bytes = new object[strLength];

            /**/
            /*每循环一次产生一个含两个元素的十六进制字节数组，并将其放入bject数组中 
                每个汉字有四个区位码组成 
                区位码第1位和区位码第2位作为字节数组第一个元素 
                区位码第3位和区位码第4位作为字节数组第二个元素 
                */
            for (int i = 0; i < strLength; i++)
            {
                //区位码第1位 
                int r1 = rnd.Next(11, 14);
                string str_r1 = rBase[r1].Trim();

                //区位码第2位 
                rnd = new Random(Guid.NewGuid().GetHashCode() + i);//更换随机数发生器的 

                //种子避免产生重复值 
                int r2;
                if (r1 == 13)
                {
                    r2 = rnd.Next(0, 7);
                }
                else
                {
                    r2 = rnd.Next(0, 16);
                }
                string str_r2 = rBase[r2].Trim();

                //区位码第3位 
                rnd = new Random(Guid.NewGuid().GetHashCode() + r2);
                int r3 = rnd.Next(10, 16);
                string str_r3 = rBase[r3].Trim();

                //区位码第4位 
                rnd = new Random(Guid.NewGuid().GetHashCode() + r3);
                int r4;
                if (r3 == 10)
                {
                    r4 = rnd.Next(1, 16);
                }
                else if (r3 == 15)
                {
                    r4 = rnd.Next(0, 15);
                }
                else
                {
                    r4 = rnd.Next(0, 16);
                }
                string str_r4 = rBase[r4].Trim();

                //定义两个字节变量存储产生的随机汉字区位码 
                byte byte1 = Convert.ToByte(str_r1 + str_r2, 16);
                byte byte2 = Convert.ToByte(str_r3 + str_r4, 16);
                //将两个字节变量存储在字节数组中 
                byte[] str_r = new byte[] { byte1, byte2 };

                //将产生的一个汉字的字节数组放入object数组中 
                bytes.SetValue(str_r, i);

            }
            Encoding gb = Encoding.GetEncoding("gb2312");
            var charList = bytes.Select(it => gb.GetString((byte[])Convert.ChangeType(it, typeof(byte[]))));
            string reval = string.Join("", charList);
            return reval;
        }

        /// <summary>
        /// 根据规则随机生成字符串 "?"代表一个字符，"#"代表一个一位数字，"*"代表一个字符串或一个一位数字
        /// </summary>
        /// <param name="pattern">样式："?"代表一个字符，"#"代表一个一位数字，"*"代表一个字符串或一个一位数字</param>
        /// <returns>随机字符串</returns>
        public string GetRandStringByPattern(string pattern)
        {
            Random rand = new Random(Guid.NewGuid().GetHashCode());
            //  Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
            if (!pattern.Contains("#") && !pattern.Contains("?") && !pattern.Contains("*"))
            {
                return pattern;
            }

            char[] nums = pattern.ToCharArray();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < nums.Length; i++)
            {
                switch (nums[i])
                {
                    case '?':
                        nums[i] = RandChar[rand.Next(10, 62)];
                        break;
                    case '#':
                        nums[i] = RandChar[rand.Next(0, 10)];
                        break;
                    case '*':
                        nums[i] = RandChar[rand.Next(62)];
                        break;
                    default:
                        break;
                }

                sb.Append(nums[i]);
            }

            return sb.ToString();
        }

        /// <summary>
        /// 生成随机的数值
        /// </summary>
        /// <param name="min">随机数可取该下界值</param>
        /// <param name="max">随机数的上界</param>
        /// <returns>随机的数值</returns>
        public int GetFormatedNumeric(int min, int max)
        {
            int num = 0;
            Random ro = new Random(unchecked(seed * (int)DateTime.Now.Ticks));
            num = ro.Next(min, max);
            seed++;
            return num;
        }

        /// <summary>
        /// 获取指定长度和字符的随机字符串
        /// 通过调用 Random 类的 Next() 方法，先获得一个大于或等于 0 而小于 pwdchars 长度的整数
        /// 以该数作为索引值，从可用字符串中随机取字符，以指定的密码长度为循环次数
        /// 依次连接取得的字符，最后即得到所需的随机密码串了。
        /// </summary>
        /// <param name="pwdchars">随机字符串里包含的字符</param>
        /// <param name="pwdlen">随机字符串的长度</param>
        /// <returns>随机产生的字符串</returns>
        public string GetRandomString(string pwdchars, int pwdlen)
        {
            // Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
            var s = unchecked((int)DateTime.Now.Ticks);
            Random rand = new Random(Guid.NewGuid().GetHashCode());
            StringBuilder tmpstr = new StringBuilder();
            int randNum;

            for (int i = 0; i < pwdlen; i++)
            {
                randNum = rand.Next(pwdchars.Length);
                tmpstr.Append(pwdchars[randNum]);
            }

            return tmpstr.ToString();
        }

        /// <summary>
        /// 获取指定长度的随机字符串
        /// </summary>
        /// <param name="pwdlen">随机字符串的长度</param>
        /// <returns>随机产生的字符串</returns>
        public string GetRandomString(int pwdlen)
        {
            return GetRandomString("abcdefghijklmnoaspqrstuvwxyzABCDEFGHIJKLMNasdfOPQRSTUVWXYZ0123456789_*", pwdlen);
        }

        /// <summary>
        /// 获取指定长度的纯字母随机字符串
        /// </summary>
        /// <param name="pwdlen">数字串长度</param>
        /// <returns>纯字母随机字符串</returns>
        public string GetRandWord(int pwdlen)
        {
            return GetRandomString("abcdefghijklmnopqrstuvwxyzdafasfaABCDEFGHIJKLMNOPQRSTUVWXYZ", pwdlen);
        }

        /// <summary>
        /// 获取指定长度的纯数字随机数字串
        /// </summary>
        /// <param name="intGuid">数字串长度</param>
        /// <returns>纯数字随机数字串</returns>
        public string GetRandomNum(int intGuid)
        {
            Random rand = new Random(Guid.NewGuid().GetHashCode());
            // Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
            StringBuilder w = new StringBuilder(string.Empty);

            for (int i = 0; i < intGuid; i++)
            {
                w.Append(rand.Next(10));
            }

            return w.ToString();
        }

        /// <summary>
        /// 获取按照年月时分秒随机数生成的文件名
        /// </summary>
        /// <returns>随机文件名</returns>
        public string GetFileRndName()
        {
            return DateTime.Now.ToString("yyyyMMddHHmmss", CultureInfo.CurrentCulture) + GetRandomString("0123456789", 4);
        }
    }





}



