﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Infrastructure.Common;

namespace Infrastructure.Common
{
    /// <summary>
    /// Http请求帮助类
    /// </summary>
    public static class HttpClientHelper
    {
        private static readonly ServiceProvider _serviceProvider =
            new ServiceCollection().AddHttpClient().BuildServiceProvider();

        private static async Task HttpClientFactoryTest()
        {
            var httpClientFactory = _serviceProvider.GetService<IHttpClientFactory>();
            var client = httpClientFactory.CreateClient();
            var response = await client.SendAsync(new HttpRequestMessage(System.Net.Http.HttpMethod.Get, "https://www.aliyun.com/"));
            var content = await response.Content.ReadAsStringAsync();

            string tmp = string.Empty;
        }

        // var url = "";
        //XnRestfulResult<object>
        public static async Task<string> HttpClient(string url, object jsonObj)
        {
            var jsonContent = jsonObj.ToJson();
            var httpClientFactory = _serviceProvider.GetService<IHttpClientFactory>();
            var client = httpClientFactory.CreateClient();

            string tmp = string.Empty;
            var contents = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            ////请求
            HttpResponseMessage response = client.PostAsync(url, contents).Result;
            if (response.IsSuccessStatusCode)
            {
                var t = await response.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(t))
                {
                    tmp = t;
                }
            }
            else
            {
                tmp = response.ToJson();
            }

            return tmp;


        }



        public static async Task<string> HttpPost(string url, object body, Dictionary<string, string> headers = null)
        {

            var httpClientFactory = _serviceProvider.GetService<IHttpClientFactory>();
            var client = httpClientFactory.CreateClient();

            if (headers != null)
            {
                foreach (var key in headers.Keys)
                {
                    client.DefaultRequestHeaders.Add(key, headers[key]);
                }
            }

            client.Timeout = TimeSpan.FromSeconds(200);
            var jsonContent = body.ToJson();
            var contents = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            ////请求
            var response = await client.PostAsync(url, contents);

            if (response.IsSuccessStatusCode)
            {
                //await response.Content.re
                var tmp = await response.Content.ReadAsStringAsync();
                if (!string.IsNullOrWhiteSpace(tmp))
                {
                    return tmp;
                }
            }
            else
            {
                return response.ToJson();
            }

            return "";

        }

    }
}
