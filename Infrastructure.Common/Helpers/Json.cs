﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;

namespace Infrastructure.Common
{
    /// <summary>
    /// Json操作
    /// </summary>
    public static partial class Json
    {
        /// <summary>
        /// 将对象转换为Json字符串
        /// </summary>
        /// <param name="target">目标对象</param>
        /// <param name="isConvertToSingleQuotes">是否将双引号转成单引号</param>
        public static string ToJson(object target)
        {
            var timeConverter = new IsoDateTimeConverter { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" };
            if (target == null)
                return string.Empty;
            var result = JsonConvert.SerializeObject(target, timeConverter);
            // var json = new JsonSerializer().Deserialize(new JsonTextReader(new StringReader(result)));
            return result;//.Replace(@"/""","");
        }

        /// <summary>
        /// 将对象转换为Json,字符串首字母小写
        /// </summary>
        /// <param name="target">目标对象</param>
        public static string ToJsonCamelCase(object target)
        {
            var timeConverter = new JsonSerializerSettings { 
                DateFormatString = "yyyy-MM-dd HH:mm:ss",
                ContractResolver = new CamelCasePropertyNamesContractResolver(),// // 首字母小写(驼峰样式)
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,// // 忽略循环引用
            };
            if (target == null)
                return string.Empty;
            var result = JsonConvert.SerializeObject(target, timeConverter);
            return result; 
        }

        public static T ToObject<T>(string json)
        {
            return string.IsNullOrWhiteSpace(json) ? default(T) : JsonConvert.DeserializeObject<T>(json);
        }

        public static object ToObject(object json, Type type)
        {
            var j = ToJson(json);
            return  JsonConvert.DeserializeObject(j, type);
        }





        public static List<T> ToList<T>(string Json)
        {
            return string.IsNullOrWhiteSpace(Json) ? null : JsonConvert.DeserializeObject<List<T>>(Json);
        }

        public static JObject ToJObject(string Json)
        {
            return string.IsNullOrWhiteSpace(Json) ? JObject.Parse("{}") : JObject.Parse(Json.Replace("&nbsp;", ""));
        }

        public static JArray ToJArry(string json)
        {
            return Json.ToObject<JArray>(json);
        }

    }
}
