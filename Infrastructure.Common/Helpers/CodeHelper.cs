﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Infrastructure.Common
{
    /// <summary>
    /// 代码生成帮助类
    /// </summary>
    public static class CodeHelper
    {
        public static string ConvertDataType(Type proType)
        {
            if (proType.IsEnum)
            {
                return "int";
            }
            var dataType = proType.ToString();
            if (string.IsNullOrEmpty(dataType)) return "";
            if (dataType.StartsWith("System.Nullable"))
                dataType = new Regex(@"(?i)(?<=\[)(.*)(?=\])").Match(dataType).Value; // 中括号[]里面值

            switch (dataType)
            {
                case "System.Guid": return "guid";
                case "System.String": return "string";
                case "System.Int32": return "int";
                case "System.Int64": return "Guid";
                case "System.Single": return "float";
                case "System.Double": return "double";
                case "System.Decimal": return "decimal";
                case "System.Boolean": return "bool";
                case "System.DateTime": return "DateTime";
                case "System.Byte": return "byte";
                case "System.Byte[]": return "byte[]";
                default:
                    break;
            }
            return dataType;
        }

        /// <summary>
        /// 数据类型转显示类型
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public static string DataTypeToEff(string dataType)
        {
            if (string.IsNullOrEmpty(dataType)) return "";
            return dataType switch
            {
                "string" => "input",
                "int" => "inputnumber",
                "Guid" => "input",
                "float" => "input",
                "double" => "input",
                "decimal" => "input",
                "bool" => "switch",
                "long" => "input",
                "DateTime" => "datepicker",
                _ => "input",
            };
        }

        // 是否通用字段
        public static bool IsCommonColumn(string columnName)
        {
            var columnList = new List<string>()
            {
                "CreatedTime", "UpdatedTime", "CreatedUserId", "CreatedUserName", "UpdatedUserId", "UpdatedUserName", "IsDeleted"
            };
            return columnList.Contains(columnName);
        }
    }



    public class CodeGuid
    {
        int _id;
        private CodeGuid() { _id = 0; }
        private static readonly CodeGuid Instance = new();

        private Guid GetUuid()
        {
            _id++;
            return GetGuid(_id);
        }

        public static Guid GetGuid()
        {
            return CodeGuid.Instance.GetUuid();
        }

        public static Guid GetGuid(int num)
        {
            var n = num.ToString();
            while (n.Length < 12)
            {
                n = "0" + n;
            }
            return new Guid("00000000-0000-0000-0000-" + n);
        }
    }
}