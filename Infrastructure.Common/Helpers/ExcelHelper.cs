﻿
using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Eval;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Infrastructure.Common.Attributes;

namespace Infrastructure.Common
{


    public class NPOIMemoryStream : MemoryStream
    {
        /// <summary>
        /// 获取流是否关闭
        /// </summary>
        public bool IsColse
        {
            get;
            private set;
        }
        public NPOIMemoryStream(bool colse = false)
        {
            IsColse = colse;
        }
        public override void Close()
        {
            if (IsColse)
            {
                base.Close();
            }
        }
    }

    public static class ExcelHelper
    {

        /// <summary>
        /// 通用导出
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static MemoryStream Export<T>(List<T> list) where T : new()
        {
            var modes = DisplayFieldMethod.GetExportColDefs<T>();
            if (!modes.Any()) { return default; }
            var result = ExportData(modes.OrderBy(x => x.Index).Select(x => x.FieldName).ToList(), list);
            var stream = ExportFile("sheet", "", modes.OrderBy(x => x.Index).Select(x => x.DisplayName).ToList(), result);
            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        public static List<List<string>> ExportData<T>(List<string> header, List<T> list)
        {
            List<List<string>> result = new List<List<string>>();
            foreach (var item in list)
            {
                List<string> olist = new List<string>();

                foreach (var a in header)
                {

                    var type = item.GetType().GetProperty(a).PropertyType;
                    var obj = item.GetType().GetProperty(a).GetValue(item);
                    if (obj == null)
                    { olist.Add(""); }
                    else
                    {
                        if (type == typeof(decimal) || type == typeof(decimal?))
                        {
                            var value = ((decimal)obj).ToString("0.000000").TrimEnd('0');
                            if (value.IndexOf('.') == value.Length - 1)
                            {
                                value = value.Replace(".", "");
                            }
                            olist.Add(value);
                        }
                        else if (type == typeof(DateTime))
                        {

                            System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.CultureInfo("en-US", false).DateTimeFormat;
                            dtfi.ShortTimePattern = "t";
                            DateTime dt = DateTime.Parse(obj.ToString(), dtfi);
                            olist.Add(DateTime.Parse(dt.ToString()).ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                        else if (type == typeof(DateTime?))
                        {
                            try
                            {
                                System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.CultureInfo("en-US", false).DateTimeFormat;
                                dtfi.ShortTimePattern = "t";
                                DateTime dt = DateTime.Parse(obj.ToString(), dtfi);
                                olist.Add(DateTime.Parse(dt.ToString()).ToString("yyyy-MM-dd HH:mm:ss"));
                            }
                            catch
                            {
                                olist.Add("");
                            }
                        }
                        else
                        {
                            olist.Add(obj.ToString());
                        }
                    }


                }
                result.Add(olist);

            }
            return result;
        }





        #region 导出excle
        public static NPOIMemoryStream ExportFile(string sheetName, string header, List<string> titleList, List<List<string>> dataList)
        {
            if (!string.IsNullOrEmpty(sheetName) && !string.IsNullOrWhiteSpace(sheetName))
            {
                if (null != titleList && titleList.Count > 0)
                {
                    #region 创建excel sheet
                    var workBook = new XSSFWorkbook();
                    var sheet = workBook.CreateSheet(sheetName);
                    #endregion
                    var initRow = 0;
                    #region 创建标头
                    if (!string.IsNullOrEmpty(header))
                    {
                        sheet.AddMergedRegion(new CellRangeAddress(0, 0, 0, titleList.Count - 1));
                        var firstRow = sheet.CreateRow(initRow++);
                        //设置单元格的高度
                        firstRow.Height = 30 * 20;
                        var firstCell = firstRow.CreateCell(0);
                        firstCell.CellStyle = GetTitleStyle(workBook);
                        firstCell.SetCellValue(header);
                    }
                    #endregion

                    #region 创建字段
                    var titleRow = sheet.CreateRow(initRow++);
                    titleRow.Height = 20 * 20;
                    var titleCellStyle = GetTitleStyle(workBook);
                    for (int i = 0; i < titleList.Count; i++)
                    {
                        var item = titleList[i];
                        var cell = titleRow.CreateCell(i);
                        //设置单元格的宽度
                        sheet.SetColumnWidth(i, 50 * 100);
                        cell.CellStyle = titleCellStyle;
                        cell.SetCellValue(item);
                    }
                    #endregion

                    #region 创建数据
                    var cellStyle = GetCellStyle(workBook);
                    for (int j = 0; j < dataList.Count; j++)
                    {
                        var list = dataList[j];
                        var row = sheet.CreateRow(j + initRow);
                        row.Height = 20 * 20;
                        for (int c = 0; c < list.Count; c++)
                        {
                            var item = list[c];
                            var cell = row.CreateCell(c);
                            cell.CellStyle = cellStyle;
                            cell.SetCellValue(item);
                        }
                    }
                    #endregion

                    #region 进行导出
                    try
                    {
                        NPOIMemoryStream ms = new NPOIMemoryStream();
                        workBook.Write(ms);
                        workBook.Close();
                        ms.Flush();
                        ms.Position = 0;

                        return ms;
                    }
                    catch (Exception exc)
                    {
                        throw exc;
                    }

                    #endregion
                }
            }
            return null;
        }

        private static ICellStyle GetFirstRowStyle(IWorkbook book)
        {
            ICellStyle cellStyle = GetBaseCellStyle(book);
            IFont font = GetBaseFont(book);
            font.Boldweight = short.MaxValue;
            font.IsBold = true;
            cellStyle.SetFont(font);
            return cellStyle;
        }

        private static ICellStyle GetTitleStyle(IWorkbook book)
        {
            ICellStyle cellStyle = GetBaseCellStyle(book);
            IFont font = GetBaseFont(book);
            font.Boldweight = short.MaxValue;
            font.IsBold = true;
            cellStyle.SetFont(font);
            return cellStyle;
        }

        private static ICellStyle GetCellStyle(IWorkbook book)
        {
            ICellStyle cellStyle = GetBaseCellStyle(book);
            IFont font = GetBaseFont(book);
            cellStyle.SetFont(font);
            return cellStyle;
        }

        private static ICellStyle GetBaseCellStyle(IWorkbook book)
        {
            ICellStyle cellStyle = book.CreateCellStyle();
            //设置单元格的样式：水平对齐居中
            cellStyle.Alignment = HorizontalAlignment.Center;
            cellStyle.VerticalAlignment = VerticalAlignment.Center;
            //cellStyle.BorderTop = BorderStyle.Thin;
            //cellStyle.BorderBottom = BorderStyle.Thin;
            //cellStyle.BorderLeft = BorderStyle.Thin;
            //cellStyle.BorderRight = BorderStyle.Thin;


            cellStyle.FillBackgroundColor = IndexedColors.RoyalBlue.Index;
            IFont font = GetBaseFont(book);
            font.Boldweight = short.MaxValue;
            font.IsBold = true;
            cellStyle.SetFont(font);

            return cellStyle;
        }

        private static IFont GetBaseFont(IWorkbook book)
        {
            IFont baseFont = book.CreateFont();
            baseFont.FontHeight = 9;
            return baseFont;
        }

        #endregion

        #region 读取excel数据

        /// <summary>
        /// 智博信导入，
        /// xlsx 转 DTO 模型  ， 
        /// 属性需要标记 DisplayFieldModel
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static IEnumerable<T> Query<T>(Stream stream) where T : new()
        {
            var res = ReadExcelDictionary(stream, 0, 1);
            //
            var cols = DisplayFieldMethod.GetExportColDefs<T>();

            foreach (var item in res.ExcelDataList)
            {
                var v = new T();
                foreach (var col in cols)
                {
                    try
                    {
                        if (item.ContainsKey(col.DisplayName))
                        {
                            object newV = null;
                            object itemValue = item[col.DisplayName];
                            if (itemValue == null || itemValue.ToString() == "")
                                continue;
                            if (col.Property.PropertyType == typeof(Guid))
                            {
                                newV = Guid.Parse(itemValue.ToString());
                            }
                            else if (col.Property.PropertyType == typeof(DateTime))
                            {
                                var vs = itemValue.ToString();
                                if (DateTime.TryParse(vs, out var _v))
                                    newV = _v;
                                else if (DateTime.TryParseExact(vs, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var _v2))
                                    newV = _v2;
                                //时间搓
                                //else if (double.TryParse(vs, out var _d))
                                //    newV = DateTimeHelper.FromOADate(_d);
                                else
                                    throw new InvalidCastException($"{vs} can't cast to datetime");
                            }
                            else if (col.Property.PropertyType == typeof(string))
                            {
                                newV = itemValue?.ToString();
                            }
                            else if (col.Property.PropertyType.IsEnum)
                            {
                                newV = System.Enum.Parse(col.Property.PropertyType, itemValue?.ToString(), true);
                            }
                            else
                                newV = Convert.ChangeType(itemValue, col.Property.PropertyType);

                            col.Property.SetValue(v, newV);
                        }
                    }
                    catch (Exception e)
                    {
                        throw new InvalidCastException($"导入字段：({col.DisplayName}) 映射({col.FieldName}), {e.Message}");
                    }
                }
                yield return v;
            }
        }






        private static IWorkbook GetWorkBook(Stream stream)
        {
            IWorkbook workbook = null;
            workbook = WorkbookFactory.Create(stream);
            return workbook;
        }



        /// <summary>
        /// 读取Excel文件流
        /// </summary>
        /// <param name="stream">文件流</param>
        /// <param name="titleNum">数据行列头所属行(索引从0开始)</param>
        /// <param name="lineNum">数据表头所占行数</param>
        /// <returns></returns>
        public static ExcelDictionaryResult ReadExcelDictionary(Stream stream, int titleNum = 1, int lineNum = 1)
        {
            ExcelDictionaryResult result = new ExcelDictionaryResult();
            result.IsLoadSuccess = false;
            try
            {
                IWorkbook workbook = GetWorkBook(stream);
                if (null != workbook)
                {
                    ISheet sheet = workbook.GetSheetAt(0);//读取到指定的sheet
                    result.SheetName = sheet.SheetName;

                    #region 标题行
                    if (titleNum > 0)
                    {
                        IRow headerRow = sheet.GetRow(0);//获取第一行，一般为表头
                        ICell headerCell = headerRow.GetCell(0);
                        if (null != headerCell)
                            result.Header = FilterTitle(headerCell + string.Empty);
                    }
                    #endregion

                    #region 参数行
                    if (titleNum > 1)
                    {
                        IRow titleRow1;
                        string title, value;
                        result.Params = new Dictionary<string, object>();
                        for (int i = 1; i < titleNum; i++)
                        {
                            titleRow1 = sheet.GetRow(i);//得到参数行
                            if (null != titleRow1)
                            {
                                for (int r = 0; r < titleRow1.Cells.Count; r = r + 2)
                                {
                                    title = titleRow1.Cells[r] + string.Empty;
                                    //参数标题为空的，均不收集相关数据
                                    if (string.IsNullOrEmpty(title) || string.IsNullOrWhiteSpace(title))
                                        continue;
                                    value = titleRow1.Cells[r + 1] + string.Empty;
                                    result.Params.Add(title, value);
                                }
                            }
                        }

                    }
                    #endregion

                    #region 列头
                    result.TitleList = new List<string>();
                    result.TitleList = GetDataColumnsHeader(sheet, titleNum, lineNum);
                    #endregion

                    result.ExcelDataList = new List<IDictionary<string, object>>();
                    //遍历读取cell(sheet.FirstRowNum + 1)
                    var firstRowCount = sheet.FirstRowNum + (2 + titleNum - 1 + lineNum - 1);//设置数据行的开始索引
                    for (int i = firstRowCount; i <= sheet.LastRowNum; i++)
                    {
                        IRow row = sheet.GetRow(i);//得到一行
                        if (null == row) { break; }

                        IDictionary<string, object> item = new Dictionary<string, object>();

                        //  for (int j = row.FirstCellNum; j < result.TitleList.Count; j++)
                        for (int j = 0; j < result.TitleList.Count; j++)
                        {
                            ICell cell = row.GetCell(j);//得到cell
                            string title = FilterTitle(result.TitleList[j]);//得到标题cell
                            if (cell != null && !string.IsNullOrEmpty(title))
                            {
                                switch (cell.CellType)
                                {
                                    case CellType.Unknown: item.Add(title, (cell + string.Empty).Trim()); break;
                                    case CellType.Blank: item.Add(title, (cell + string.Empty).Trim()); continue;//emptyIndex++;
                                    case CellType.Boolean: item.Add(title, (cell.BooleanCellValue + string.Empty).Trim()); break;
                                    case CellType.String: item.Add(title, (cell.StringCellValue + string.Empty).Trim()); break;
                                    case CellType.Error: item.Add(title, ErrorEval.GetText(cell.ErrorCellValue).Trim()); break;
                                    case CellType.Numeric:
                                        if (DateUtil.IsCellDateFormatted(cell))
                                        {
                                            var dt = cell.DateCellValue;
                                            if (DateTime.MinValue != dt)
                                                item.Add(title, dt.ToString("yyyy-MM-dd HH:mm:ss").Trim());
                                        }
                                        else { item.Add(title, (cell.NumericCellValue + string.Empty).Trim()); }
                                        break;
                                    case CellType.Formula:
                                        switch (cell.CachedFormulaResultType)
                                        {
                                            case CellType.Unknown: item.Add(title, (cell.CellFormula + string.Empty).Trim()); break;
                                            case CellType.Blank: item.Add(title, (cell + string.Empty).Trim()); continue;//
                                            case CellType.Boolean: item.Add(title, (cell.BooleanCellValue + string.Empty).Trim()); break;
                                            case CellType.String: item.Add(title, (cell.StringCellValue + string.Empty).Trim()); break;
                                            case CellType.Error: item.Add(title, ErrorEval.GetText(cell.ErrorCellValue).Trim()); break;
                                            case CellType.Numeric:
                                                if (DateUtil.IsCellDateFormatted(cell))
                                                {
                                                    var dt = cell.DateCellValue;
                                                    if (null != dt && DateTime.MinValue != dt)
                                                        item.Add(title, dt.ToString("yyyy-MM-dd HH:mm:ss").Trim());
                                                }
                                                else { item.Add(title, (cell.NumericCellValue + string.Empty).Trim()); }
                                                break;
                                        }
                                        break;
                                    default:
                                        
                                        break;
                                }
                            }
                            else
                            {
                                item.Add(title, (string.Empty + string.Empty).Trim());
                            }
                        }
                        //if (emptyIndex < result.TitleList.Count) 
                        //{
                        if (item.Values.Count >= result.TitleList.Count)
                        {
                            if (item.Values.Count(s => string.IsNullOrEmpty(s + string.Empty)) < result.TitleList.Count)
                            {
                                result.ExcelDataList.Add(item);
                            }
                        }
                        // }

                    }
                    result.IsLoadSuccess = result.ExcelDataList.Count > 0;
                }
            }
            catch (Exception exc)
            {
                result.IsLoadSuccess = false;
                result.Message = exc.Message;
                //throw exc;
            }
            finally
            {
                stream.Close();
                stream.Dispose();
                stream = null;
            }
            return result;
        }

        /// <summary>
        /// 获取excel数据列列头
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="titleNum">数据表头索引行</param>
        /// <param name="lineNum">数据表头所占行数</param>
        /// <returns></returns>
        private static List<string> GetDataColumnsHeader(ISheet sheet, int titleNum = 1, int lineNum = 1)
        {
            List<string> headers = new List<string>();
            Dictionary<int, string> dic = new Dictionary<int, string>();
            IRow titleRow;//得到属性名字行
            int num = 0;
            for (int i = 0; i < lineNum; i++)
            {
                titleRow = sheet.GetRow(titleNum + i);//得到属性名字行
                if (null != titleRow)
                {
                    num = 0;
                    foreach (ICell item in titleRow.Cells)
                    {
                        string title = item + string.Empty;
                        if (!string.IsNullOrEmpty(title) && !string.IsNullOrWhiteSpace(title))
                        {
                            dic.Remove(num);
                            dic.Add(num, title);
                        }
                        num++;
                    }
                }
            }
            foreach (var item in dic.OrderBy(d => d.Key))
            {
                headers.Add(item.Value);
            }
            return headers;
        }

        /// <summary>
        /// 过滤标题列符号
        /// </summary>
        /// <param name="title">标题</param>
        /// <returns></returns>
        public static string FilterTitle(string title)
        {
            if (!string.IsNullOrEmpty(title) && !string.IsNullOrWhiteSpace(title))
            {
                title = title.Replace("*", string.Empty).Trim();
                title = Regex.Replace(title, @"\s{1,}", string.Empty, RegexOptions.IgnoreCase).Trim();
                title = Regex.Replace(title, @"\n\s*\r\n", string.Empty, RegexOptions.IgnoreCase).Trim();
                return title.Trim();
            }
            return string.Empty;
        }

        #endregion


    }



    public class ExcelDictionaryResult : ExcelResultBase
    {
        public List<IDictionary<string, object>> ExcelDataList { get; set; }
    }

    public class ExcelResultBase
    {
        public bool IsLoadSuccess { get; set; }

        public string SheetName { get; set; }

        public string Message { get; set; }

        public string Header { get; set; }

        public List<string> TitleList { get; set; }

        public Dictionary<string, object> Params { get; set; }
    }
}
