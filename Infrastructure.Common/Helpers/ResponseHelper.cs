﻿using Infrastructure.Common;

namespace Infrastructure.Common
{
    /// <summary>
    /// Api 返回参数
    /// </summary>
    public static class ResponseHelper
    {
        public static IResultModel<object> Success(object obj = null, string successMsg = "成功")
        {
            return Success<object>(obj);
        }
        public static IResultModel<T> Success<T>(T t, string successMsg = "成功")
        {
            return new ResultModel<T>
            {
                Data = t,
                Message = successMsg
            };
        }

        public static IResultModel<object> Bool(bool bl, string successMsg = "成功", string errorMsg = "失败")
        {
            return bl ? Success(null, successMsg) : Error(errorMsg);
        }

        /// <summary>
        /// 错误结果
        /// </summary>
        /// <param name="errorMsg"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public static IResultModel<object> Error(string errorMsg = "失败", int statusCode = 300)
        {
            return Error<object>(errorMsg, statusCode);
        }

        /// <summary>
        /// 错误结果
        /// </summary>
        /// <param name="errorMsg"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public static IResultModel<object> Error<T>(string errorMsg = "失败", int statusCode = 300)
        {
            return new ResultModel<object>
            {
                Success = false,
                Message = errorMsg,
                Code = statusCode
            };
        }

        /// <summary>
        /// 参数错误
        /// </summary>
        /// <returns></returns>
        public static IResultModel<object> ErrorParam => Error("参数错误");

    }
}
