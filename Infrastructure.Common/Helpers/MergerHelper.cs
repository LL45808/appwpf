﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Common
{
    public class MergerHelper
    {

        private static PropertyInfo[] GetObjectProperties<Dto>()
        {
            var type = typeof(Dto);
            var key = type.TypeHandle;
            return type.GetProperties();
        }


        /// <summary>
        /// 合并对象
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="source"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static TSource MergerObject<TSource, TTarget>(TSource source, TTarget t)
        {
            var ps = GetObjectProperties<TSource>();
            var ta = GetObjectProperties<TTarget>();
            foreach (var item in ps)
            {
                if (!item.CanWrite)
                {
                    continue;
                }
                var obj = ta.FirstOrDefault(x => x.Name == item.Name);
                var k = obj?.GetValue(t, null);
                if (k != null)
                {
                    item.SetValue(source, k);
                    continue;
                }
            }

            return source;
        }



        /// <summary>
        /// 合并2个对象
        /// </summary>
        /// <typeparam name="TSource">对象1类型</typeparam>
        /// <typeparam name="TTarget">对象2类型</typeparam>
        /// <param name="s">对象1实例</param>
        /// <param name="t">对象2实例</param>
        /// <returns>合并后的动态对象</returns>
        public static TSource MergerObject<TSource, TTarget, TTarget2>(TSource source, TTarget2 s, TTarget t)
        {

            try
            {

                var ps = GetObjectProperties<TSource>();
                var targetPts = GetObjectProperties<TTarget2>();
                var ta = GetObjectProperties<TTarget>();

                foreach (var item in ps)
                {
                    if (!item.CanWrite)
                    {
                        continue;
                    }
                    var obj = targetPts.FirstOrDefault(x => x.Name == item.Name);
                    var k = obj?.GetValue(s, null);
                    if (k != null)
                    {
                        item.SetValue(source, k);
                        continue;
                    }

                    obj = ta.FirstOrDefault(x => x.Name == item.Name);
                    k = obj?.GetValue(t, null);

                    if (k != null)
                    {
                        item.SetValue(source, k);
                        continue;
                    }

                }
            }
            catch (Exception e)
            {

                throw;
            }


            return source;
        }

    }
}
