﻿using System;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace Infrastructure.Common
{
    /// <summary>
    /// 配置文件帮助类、获取appsetting中的配置
    /// </summary>
    public static class ConfigHelper
    {
        private static IConfiguration _config;
        private static object _lock = new object();
        public static IConfiguration Configuration
        {
            get
            {
                if (_config == null)
                {
                    lock (_lock)
                    {
                        if (_config == null)
                        {
                           
                            try
                            {
                                var builder = new ConfigurationBuilder()
                                                   .SetBasePath(AppContext.BaseDirectory)
                                                   .AddJsonFile("appsettings.json");
                                _config = builder.Build();
                            }
                            catch  
                            {
                                var ppk = Path.GetFullPath(@"..//..//..")+"//";
                                var builder = new ConfigurationBuilder()
                                                   .SetBasePath(ppk)
                                                   .AddJsonFile("appsettings.json");
                                _config = builder.Build();
                            }
                           
                        }
                    }
                }

                return _config;
            }
            set
            {
                _config = value;
            }
        }

        /// <summary>
        /// 从AppSettings获取key的值
        /// "key:key:key"
        /// </summary>
        /// <param name="key">key</param>
        /// <returns></returns>
        public static string GetValue(string key)
        {
            return Configuration[key];
        }


        /// <summary>
        /// 从AppSettings获取key为 T类名 的值的对象
        /// 不设置为默认
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetObj<T>() where T : new()
        {
            try
            {
                var key = typeof(T).Name;
                var newt = new T();
                Configuration.GetSection(key).Bind(newt);
                return newt;
            }
            catch
            {
                return default;
            }
        }



        /// <summary>
        /// 获取连接字符串
        /// </summary>
        /// <param name="nameOfCon">连接字符串名</param>
        /// <returns></returns>
        public static string GetConnectionString(string nameOfCon)
        {
            return Configuration.GetConnectionString(nameOfCon);
        }

        /// <summary>
        /// 获取默认字符串
        /// </summary>
        /// <param name="nameOfCon">连接字符串名</param>
        /// <returns></returns>
        public static string GetDefaultConnectionString()
        {
            return GetConnectionString(GetConnectionString("Default"));
        }


    }


}
