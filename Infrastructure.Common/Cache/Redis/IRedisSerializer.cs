﻿using System;
using System.Collections.Generic;
using System.Text;
using Infrastructure.Common;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Infrastructure.Common 
{
    /// <summary>
    /// Redis序列化器
    /// </summary>
    public interface IRedisSerializer
    {
        /// <summary>
        /// 序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        string Serialize<T>(T value);

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        T Deserialize<T>(RedisValue value);

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        object Deserialize(RedisValue value, Type type);
    }
    
    public class RedisSerializer : IRedisSerializer
    {
        public string Serialize<T>(T value)
        {
            if (IsNotBaseType<T>())
            {
                return JsonConvert.SerializeObject(value);
            }

            return value.ToString();
        }

        public T Deserialize<T>(RedisValue value)
        {
            if (IsNotBaseType<T>())
            {
                return JsonConvert.DeserializeObject<T>(value);
            }

            return value.To<T>();
        }

        public object Deserialize(RedisValue value, Type type)
        {
            if (Type.GetTypeCode(type) == TypeCode.Object)
            {
                return JsonConvert.DeserializeObject(value, type);
            }

            return value;
        }

        /// <summary>
        /// 是否是基础类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        private bool IsNotBaseType<T>()
        {
            return Type.GetTypeCode(typeof(T)) == TypeCode.Object;
        }
    }
}
