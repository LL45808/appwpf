﻿
using Infrastructure.Common;

namespace Infrastructure.Common 
{
    //EasyCaching
    public class CacheOption
    {
        /// <summary>
        /// 缓存提供器
        /// </summary>
        public CacheProvider Provider { get; set; } = CacheProvider.Redis;

        public RedisConfig Redis { get; set; } = new();

    }

    public class RedisConfig
    {
        public string Host { get; set; } = "47.108.230.12";
        public string Port { get; set; } = "7379";
        public string Pwd { get; set; } = "redismima";//requirepass

        /// <summary>
        /// 默认数据库
        /// </summary>
        public int DefaultDb { get; set; } = 3;

        /// <summary>
        /// 前缀，用于区分不用的项目
        /// </summary>
        public string Prefix { get; set; } = "zbx_admin";

        /// <summary>
        /// 连接字符串
        /// </summary>
        public string ConnectionString
        {
            //127.0.0.1:6379,password=123456
            get
            {
                var url = $"{Host}:{Port}";
                if (Pwd.IsNullOrEmpty())
                {
                    return url;
                }
                return url + ",password=" + Pwd;
            }
        }

    }


}
