﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Caching.Memory;

namespace Infrastructure.Common 
{
    /// <summary>
    /// 
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="services"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public static IServiceCollection AddCacheHandler(this IServiceCollection services, CacheOption config)
        {
            // var config = ConfigHelper.GetObj<CacheOption>();
            if (config.Provider == CacheProvider.Redis)
            {
                services.AddSingleton<IRedisSerializer, RedisSerializer>();
                services.AddSingleton<RedisHelper>();
                services.AddSingleton<ICacheHandler, RedisCacheHandler>();
            }
            else
            {
                // services.AddMemoryCache();
                services.AddSingleton<ICacheHandler, MemoryCacheHandler>();
            }

            return services;
        }
    }
}
