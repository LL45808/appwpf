﻿using AppClient.ViewModels;
using AppClient.Views;
using Microsoft.Extensions.DependencyInjection;
using Rubyer;
using ShowMeTheXAML;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace AppClient
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Services = ConfigureServices();
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;


            XamlDisplay.Init();
            if (new Login().ShowDialog() == false)
            {
                new MainWindow().ShowDialog();
            }
            Current.Shutdown();
        }

        /// <summary>
        /// Gets the current <see cref="App"/> instance in use
        /// </summary>
        public new static App Current => (App)Application.Current;

        /// <summary>
        /// Gets the <see cref="IServiceProvider"/> instance to resolve application services.
        /// </summary>
        public IServiceProvider Services { get; }


        /// <summary>
        /// Configures the services for the application.
        /// </summary>
        private static IServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection();
            // View models
            services.AddSingleton<LoginViewModel>();
            //services.AddSingleton<IconViewModel>();
            //services.AddSingleton<ListViewModel>();
            //services.AddSingleton<DataGridViewModel>();
            //services.AddSingleton<TabControlViewModel>();
            //services.AddSingleton<PageBarViewModel>();
            //services.AddSingleton<MessageBoxViewModel>();
            //services.AddSingleton<DialogViewModel>();
            //services.AddSingleton<StepBarViewModel>();
            //services.AddSingleton<DescriptionViewModel>();
            //services.AddSingleton<HamburgerMenuViewModel>();
            //services.AddSingleton<TextBoxViewModel>();
            //services.AddSingleton<PasswordBoxViewModel>();
            //services.AddSingleton<NumericBoxViewModel>();
            //services.AddSingleton<ComboBoxViewModel>();
            //services.AddSingleton<RenamerViewModel>();
            //services.AddSingleton<DateTimePickerViewModel>();
            //services.AddSingleton<GroupBoxViewModel>();
            //services.AddSingleton<ExpanderViewModel>();
            //services.AddSingleton<ListBoxViewModel>();
            //services.AddSingleton<TreeViewViewModel>();
            //services.AddSingleton<FlipViewViewModel>();
            //services.AddSingleton<TreeDataGridViewModel>();

            //services.AddSingleton<WechatViewModel>();
            //services.AddSingleton<NetEaseCloudMusicViewModel>();

            return services.BuildServiceProvider();
        }





        private void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            var res = MessageBoxR.ErrorGlobal(e.Exception.Message, title: "发生异常", button: MessageBoxButton.YesNoCancel);
            
            //处理UI线程上的未处理异常
            //  MessageBox.Show("发生错误：" + e.Exception.Message, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            //处理非UI线程上的未处理异常
            Exception ex = (Exception)e.ExceptionObject;
            MessageBox.Show("发生错误：" + ex.Message, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Task线程内未捕获异常处理事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TaskScheduler_UnobservedTaskException(object? sender, UnobservedTaskExceptionEventArgs ex)
        {
            MessageBox.Show("Task线程异常：" + ex.Exception.Message, "错误", MessageBoxButton.OK, MessageBoxImage.Error);
            //设置该异常已察觉（这样处理后就不会引起程序崩溃）
            ex.SetObserved();
        }




    }
}
