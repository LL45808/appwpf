﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using Rubyer;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AppClient.ViewModels
{
    public partial class PageViewModel
    {
        /// <summary>
        /// 下一步命令
        /// </summary>
        [RelayCommand]
        private void Selecte(object message)
        {
            Debug.WriteLine($"HamburgerMenuItem Command: {message}");
        }

    }
}
